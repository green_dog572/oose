package �2;

public class a1 {

	// A1) Fakult�tsfunktion
	public static long fac(long n) {
		if(n <= 1) return 1; 
		return n * fac(n - 1);
	}
	
	// A2) Binomialkoeffizient
	public static long bink(long n, long k) {
		if(k > n && n >= 0) {
			return 0;
		}
		long res = 1;
		// Berechne res = n(n-1)...(n - k + 1)
		for(long i = 0; i <= n - k + 1; i++) {
			res *= n - i;
		}
		// Berechne res / (1 * 2 * ... * k) = res / fac(k)
		res /= fac(k);
		return res;
	}
	
	// A3) Modifiziertes Lottoproblem
	public static long modLotProb(long n, long k) {
		long res = 1;
		res *= bink(n, k);
		res *= fac(k);
		return res;
	}
	
	// Alternative zu A3
	public static long modLotProbAlt(long n, long k) {
		long res = 1;
		for(long i = 0; i <= n - k + 1; i++) {
			res *= n - i;
		}
		return res;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(fac(6));	// Sollte 720 ausgeben
		System.out.println(bink(10, 6)); // Sollte 210 ausgeben
		System.out.println(modLotProb(49, 6));
		System.out.println(modLotProbAlt(10, 6));
	}

}
