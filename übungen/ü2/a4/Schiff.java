package �2.a4;

public class Schiff {
	private String _Name, _Kennzeichen;
	private double _L�nge;
	
	Schiff(String Name, double L�nge) {
		_Name = Name;
		_L�nge = L�nge;
	}
	
	void setKennzeichen(String Kennzeichen) {
		_Kennzeichen = Kennzeichen;
	}
	
	void empfangeNachricht(String msg) {
		System.out.println(msg + " an: " + _Name + ", " + _Kennzeichen);
	}
	
}
