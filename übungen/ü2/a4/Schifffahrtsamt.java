package �2.a4;

public class Schifffahrtsamt {
	Schiff[] _SchiffsDB = new Schiff[50];
	int _SchiffeInDB;
	
	Schifffahrtsamt() { _SchiffeInDB = 0; }
	
	void regestriereSchiff(Schiff s) {
		_SchiffsDB[_SchiffeInDB] = s;
		_SchiffeInDB++;
	}
	
	void meldung(String nachricht, int dringlichkeit) {
		if(dringlichkeit == 1) {
			for(int i = 0; i < _SchiffeInDB; i++) {
				/*
				 * Sollen wir die Nachricht �bergeben und ausgeben, oder nur 
				 * an die message variable senden? Auf der Aufgabe steht nicht, dass 
				 * wir setter Methoden f�r die Variablen schreiben sollen
				 */
				_SchiffsDB[i].empfangeNachricht(nachricht);	
			}
		}
	}
	
	public static String getKennzeichen() {
		java.util.Random rnd = new java.util.Random();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < 6; i++) {
			sb.append((char)((rnd.nextInt(25)+65)));
		}
		return sb.toString();
	}
	
}
