package �2;
import java.math.BigDecimal;

public class a2 {

	public static void main(String[] args) {
		boolean x1 = (6.4f == 6.4);	// Sollte false sein
		boolean x2 = (6.5f == 6.5);	// Sollte true sein
		System.out.println(6.4 + "f == " + 6.4 + "? " + x1);
		System.out.println(6.5 + "f == " + 6.5 + "? " + x2);
		
		/*
		 * Flie�kommazahlen folgen dem IEEE-754 Standard. Dieser verwendet inverse Zweierpotenzen
		 * um Br�che wie 0.4 darzustellen. Weil 0.4 nicht als Summe von inversen
		 * Zwierpotenzen darstellbar ist, kann dieser nur approximitiert werden.
		 * 
		 * Der Float-Spezifizierer "f" deutet an, dass die Zahl als float interpretiert werden soll.
		 * Wird dieser weggelassen, ist der Typ double. Der Grund daf�r, dass 6.4f != 6.4 gilt, ist
		 * dass 6.4 genauer dargestellt werden kann, als 6.4f (und sie somit ungleich sind).
		 * 
		 * Siehe
		 */
		
		System.out.println("Double 6.4 = " + new BigDecimal(6.4));
		System.out.println("Float 6.4 = " + new BigDecimal(6.4f));
		
		/*
		 * Im Vergleich (6.4f == 6.4) wird der ungenauere Typ (float) zum Genaueren(double) gecastet.
		 * Die zwei Approximationen sind dabei ungleich, was das Ergebnis erkl�rt.
		 */
	}

}
