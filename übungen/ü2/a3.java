package �2;

public class a3 {

	public static int euklid(int a, int b) {
		if(a == 0) return b;
		while(b != 0) {
			if(a > b) a -= b;
			else b -= a;
		}
		return a;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(euklid(124, 12)); // 4
		System.out.println(euklid(24, 6));	 // 6
		System.out.println(euklid(17, 5));	 // 1
		System.out.println(euklid(9, 6));	 // 3
	}

}
